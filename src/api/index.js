const express = require('express');
const router = express.Router();
const reg = require('./registration');
// const login = require('./login');

// router.use('/login', login);
router.use('/user', reg);
module.exports = router;