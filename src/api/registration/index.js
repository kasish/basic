const express = require('express');
const router = express.Router();
const controller = require('./registration.controller');
const service = require('../../services/user.service');

// router.route('/data').get(service.dbConnection);
router.route('/data').get(service.getData);
router.route('/create').post(service.createData);
router.route('/update/:id').put(service.updateData);
router.route('/delete/:id').delete(service.deleteData);

// router.route('/').get(controller.getUser);
// router.route('/create').post(controller.createUser);
// router.route('/:id').get(controller.getUserById);

router.route('/r').get(controller.getUserBy);
router.route('/c').post(controller.createUserBy);
router.route('/u/:id').put(controller.updateUserById);
router.route('/d/:id').delete(controller.deleteUserById);

module.exports = router; 