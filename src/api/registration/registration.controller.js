const httpStatus = require('http-status');
// const userService = require('../../services/user.service');
const fs = require('fs');
const user = require("../../assets/user.json");

exports.getUserBy = async (req, res, next) => {
    // console.log("hdhsdjb");
    const dataPath = "src/assets/user.json";

    // userService.getUser().then((data) => {
    //     return (res.json(JSON.parse(data)));
    // } ).console.error();

    // const users = await userService.getUser();

    fs.readFile(dataPath, "utf8", (err, user) => {
        if (err) {
            throw err;
        }
        // console.log(data); 
        return (res.json(JSON.parse(user)));
    })
}

exports.createUserBy = async (req, res, next) => {
    const value = req.body;
    user.push(value);
    const dataPath = "src/assets/user.json";
    fs.writeFile(dataPath, JSON.stringify(user, null, 2), "utf8", err => {
        if (err) {
            throw err;
        }
        return (res.json({ "data": "new user" }));
    });
}

exports.updateUserById = async (req, res, next) => {
    const id = req.params.id;
    const body = req.body;
    const dataPath = "src/assets/user.json";
    fs.readFile(dataPath, "utf8", (err, user) => {
        user = JSON.parse(user);
        user[id].userName = body.userName;
        user[id].emailid = body.emailid;
        fs.writeFile(dataPath, JSON.stringify(user, null, 2), "utf8", (err) => {
            if (err) throw err;
        });
        return (res.json({ "data": "updated" }));
    });
}

exports.deleteUserById = async (req, res, next) => {
    const id = req.params.id;
    const dataPath = "src/assets/user.json";
    fs.readFile(dataPath, "utf8", (err, user) => {
        user = JSON.parse(user);
        user.splice(id, 1);
        fs.writeFile(dataPath, JSON.stringify(user, null, 2), "utf8", (err) => {
            if (err) throw err;
        });
        return (res.json({ "data": "deleted" }));
    });
}

exports.getUser = async (req, res, next) => {
    try {
        const data = {
            data: [{
                userName: 'Kasish',
                emailid: 'kas@va'
            }]
        };
        res.status(httpStatus.OK);
        return (res.json(data));
    }
    catch (error) {
        console.error(error);
    }
}

exports.createUser = async (req, res, next) => {
    const body = req.body;
    try {
        const data = {
            data: [{
                userName: 'Kasish',
                emailid: 'kas@va'
            }]
        };
        res.status(httpStatus.OK);
        return (res.json(data));
    }
    catch (error) {
        console.error(error);
    }
}

exports.getUserById = async (req, res, next) => {
    const body = req.body;
    try {
        const data = {
            data: [{
                userName: 'Kasish ag',
                emailid: 'kas@val'
            }]
        };
        res.status(httpStatus.OK);
        return (res.json(data));
    }
    catch (error) {
        console.error(error);
    }
}




