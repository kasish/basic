const app = require('./middleware/startup.middleware');
const port = 3000;
app.listen(port, () => {
    console.info(`myApplication runs on ${port}`);
})
module.exports = app;



