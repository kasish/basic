var mysql = require('mysql');

exports.dbConnection = async (req, res, next) => {
    var con = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'root',
        database: 'demo_db',
        port: 3307,
    });
    await con.connect((err) => {
        if (!err) console.log('Connected the mysql database');
        else {
            console.log('Error connecting database demo_db');
            console.log(err);
        }
    });
    return con;
};
    // myQuery='SELECT * FROM user';
    // con.query(myQuery, (err, results, fields) => {
    //     if (err) throw err;
    //     console.log('The solution is: ', results);
    //   });
    // con.end();


exports.dbDisconnection = async (req, res, next, con) => {
    if(con !== null && con !==undefined)
    con.end();
    console.log("DB Connection Ended........");
}