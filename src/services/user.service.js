const { dbConnection, dbDisconnection } = require('./mysql.db.service')

exports.getData = async (req, res, next) => {
  const dbconnection = await dbConnection();
  dbconnection.query('SELECT * FROM user', (err, results, fields) =>{
    // dbconnection.query('SELECT * FROM user')
    // .then(data => {
    //   resolve(data)
    // })
    // .catch(error => {
    //   reject(error)
    // })
    // .finally(() => {
    //  dbDisconnection();
    // })
    // const result = await dbconnection.query('SELECT * FROM user')
    if (err) throw err;
    console.log('....................................GET..................................');
    console.log('The solution is: ', results);
    console.log('..........................................................................');
    res.json(results);
    dbDisconnection();
  });
};

exports.createData = async (req, res, next) => {
  const dbconnection = await dbConnection();
  const value = req.body;
  dbconnection.query('INSERT INTO user (userName, emailid) VALUES (?, ?)', [value.userName, value.emailid], (err, results, fields) => {
    if (err) throw err;
    console.log('....................................CREATE................................');
    console.log('The solution is: ', results);
    console.log('..........................................................................');
    return (res.json("New User Created"));
  });

};

exports.updateData = async (req, res, next) => {
  const dbconnection = await dbConnection();
  const id = req.params.id;
  const body = req.body;
  dbconnection.query('UPDATE user SET userName = ?, emailid = ? WHERE id = ?', [body.userName, body.emailid, id], (err, results, fields) => {
    if (err) throw err;
    console.log('....................................UPDATE................................');
    console.log('The solution is: ', results);
    console.log('..........................................................................');
    return (res.json({ "Updated user with id:": id }));
  });

};

exports.deleteData = async (req, res, next) => {
  const dbconnection = await dbConnection();
  const id = req.params.id;
  dbconnection.query('DELETE FROM user WHERE id = ?', [id], (err, results, fields) => {
    if (err) throw err;
    console.log('....................................DELETE................................');
    console.log('The solution is: ', results);
    console.log('..........................................................................');
    return (res.json({ "Deleted the User with id:": id }));
  });

};


// exports.getUser = async() => {
//     const users = { "name": "Kasish" , "email": "kas@vl" }
//     return users;
// }